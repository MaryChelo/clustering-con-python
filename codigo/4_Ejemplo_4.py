#Clustering con dataset de iris

#Se importan las librerías necesarias
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn
from sklearn.cluster import KMeans

#Se hace la lectura del dataset y se almacenan en un data frame
datos = pd.read_csv("../dataset/iris/iris.csv")

#Se muestran 5 registros del data frame con el método head
datos.head()

#Se hace el clustering con el método de k-means donde se le pasa como 
#parámetro el número clusters y con el método fit se le pasa como parámetro 
#los datos de las primeras 4 columnas
clusters = KMeans(n_clusters=3)
clusters.fit(datos[["Sepal.Length","Sepal.Width","Petal.Length", "Petal.Width"]])

#Se obtienen las etiquetas de los clusters
etiquetas = clusters.labels_
etiquetas

#Se obtienen los centroides de los clusters
clusters_centroides = clusters.cluster_centers_
clusters_centroides

#Se grafican los clusters
colores = np.array(["darkred", "darkblue", "darkgreen"])
plt.scatter(x=datos["Petal.Length"], y=datos["Petal.Width"], s=25, c=colores[clusters.labels_])
plt.title("K-Means")

#Se grafican los clusters con cada uno de sus centroides
colores = np.array(["darkred", "darkblue", "darkgreen"])
for i in range(0, len(etiquetas)):
    plt.scatter(x=datos["Petal.Length"][i], y=datos["Petal.Width"][i], s=25, c=colores[etiquetas[i]])

for i in range(0, len(clusters_centroides)):
    plt.scatter(x=clusters_centroides[i,[2]], y=clusters_centroides[i,[3]], s=200, c=colores[i])
plt.title("K-Means")
plt.xlabel("Eje X")
plt.ylabel("Eje Y")



