#Clustering con el método make_blobs

#Se importan las librerías necesarias
from sklearn.cluster import AffinityPropagation
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
import matplotlib.pyplot as plt
from itertools import cycle

# Se hace la selección de los centroides y se hace el clustering con el método make_blobs2. 
#Los datos aleatorios se almacenan en la variable X y las etiquetas se almacenan en la variable etiquetas_1
centroides = [[1, 1], [-1, -1], [1, -1], [2, 1]]
X, etiquetas_1 = make_blobs(n_samples=400, centers=centroides, cluster_std=0.5, random_state=0)

#Se muestran los datos aleatorios
X

#Se muestran las etiquetas
etiquetas_1

#Se selecciona un número determinado de clusters para los datos
af = AffinityPropagation(preference=-50).fit(X)
indices_clusters = af.cluster_centers_indices_
etiquetas_2 = af.labels_

# Se obtiene el número de clusters
numero_clusters = len(indices_clusters)

# Se muestran algunas métricas
print('Número estimado de clusters: %d' % numero_clusters)
print("Homogeneidad: %0.3f" % metrics.homogeneity_score(etiquetas_1, etiquetas_2))
print("Lo completo: %0.3f" % metrics.completeness_score(etiquetas_1, etiquetas_2))
print("V-medida: %0.3f" % metrics.v_measure_score(etiquetas_1, etiquetas_2))
print("Índice de Rand ajustado: %0.3f"
      % metrics.adjusted_rand_score(etiquetas_1, etiquetas_2))
print("Información mutua ajustada: %0.3f"
      % metrics.adjusted_mutual_info_score(etiquetas_1, etiquetas_2))
print("Coeficiente de silueta: %0.3f"
      % metrics.silhouette_score(X, etiquetas_2, metric='sqeuclidean'))

#Se grafican los clusters
colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
for k, col in zip(range(numero_clusters), colors):
    clase_miembros = etiquetas_2 == k
    centro_cluster = X[indices_clusters[k]]
    plt.plot(X[clase_miembros, 0], X[clase_miembros, 1], col + '.') # puntos
    plt.plot(centro_cluster[0], centro_cluster[1], 'o', markerfacecolor=col, markeredgecolor='k', markersize=14) #centroide
    for x in X[clase_miembros]:
        plt.plot([centro_cluster[0], x[0]], [centro_cluster[1], x[1]], col) #lineas

plt.title('Número estimado de clusters: %d' % numero_clusters)
plt.show()
